<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FaqtattooController extends AbstractController
{
    /**
     * @Route("/faqtattoo", name="faqtattoo")
     */
    public function index()
    {
        return $this->render('faqtattoo/index.html.twig', [
            'controller_name' => 'FaqtattooController',
        ]);
    }
}
