<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class QsnController extends AbstractController
{
    /**
     * @Route("/qui-sommes-nous", name="qsn")
     */
    public function index()
    {
        return $this->render('qsn/qsn.html.twig', [
            'controller_name' => 'QsnController',
        ]);
    }
}
