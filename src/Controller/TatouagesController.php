<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TatouagesController extends AbstractController
{
    /**
     * @Route("/tatouages", name="tatouages")
     */
    public function index()
    {
        return $this->render('tatouages/index.html.twig', [
            'controller_name' => 'TatouagesController',
        ]);
    }
}
