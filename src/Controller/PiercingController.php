<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PiercingController extends AbstractController
{
    /**
     * @Route("/piercing", name="piercing")
     */
    public function index()
    {
        return $this->render('piercing/index.html.twig', [
            'controller_name' => 'PiercingController',
        ]);
    }
}
