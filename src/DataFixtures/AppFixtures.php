<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use Faker\Factory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      $faker = Factory::create('fr-FR');

        for ($i = 0; $i <= 2; $i++)
        {
            $admin = new Admin();
            $admin->setEmail($faker->email);
            $admin->setPassword($faker->password);
            $admin->setPseudo($faker->userName);
            $manager->persist($admin);
        }

        $manager->flush();
    }
}
