<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* qsn/qsn.html.twig */
class __TwigTemplate_a0af3dc00a6577659cb468de7a3e7478c704acf12edf3530df1c51cdd1fb8c4b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "qsn/qsn.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "qsn/qsn.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "qsn/qsn.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello QsnController!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
<section class=\"ctnr-qsn\">
    
    <div class=\"row qsn-titre\">
        <div class=\"col-12\">
            <h1 class=\"titre-qsn\">Qui sommes nous ?</h1>
        </div>
    </div>
    
    <div class=\"row zone-perceuse\">
        <div class=\"col-md-6\">
            <img src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/mow-tatoueuse.jpeg"), "html", null, true);
        echo "\" alt=\"Mow\" width=400 />
        </div>
        <div class=\"col-md-6 zone-texte-perceuse\">
            <h2 class=\"titre-perceuse\">Cécyl</h2>
            <p class=\"presentation-perceuse\">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam facilis explicabo modi, quas nulla natus, quidem qui culpa commodi illo eum atque, sed exercitationem reiciendis perspiciatis voluptatibus temporibus repudiandae enim! Vel doloremque est minima vero ipsa dolorum officia, fuga culpa sapiente temporibus beatae nam vitae et amet cum architecto velit. Nesciunt fuga tempore placeat delectus quos velit expedita exercitationem mollitia accusamus porro. Accusamus voluptatum numquam modi veritatis sit rerum officiis facere iste et quam repellat doloremque tempora nesciunt recusandae obcaecati expedita harum asperiores quis, in iure consequatur pariatur porro sed. Aliquam ipsa praesentium reprehenderit ea odio quia mollitia ipsum. Rem.</p>
        </div>
    </div>

    <div class=\"galerie1\"></div>

    <div class=\"row\">
        <div class=\"col-md-6\">
            <img src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/facebook.png"), "html", null, true);
        echo "\" alt=\"Mow\" width=60 />
        </div>
        <div class=\"col-md-6\">
            <img src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/instagram.png"), "html", null, true);
        echo "\" alt=\"Mow\" width=60 />
        </div>
    </div>
    
    <div class=\"row\">
        <div class=\"col-md-6\">
            <img src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/mow-tatoueuse.jpeg"), "html", null, true);
        echo "\" alt=\"Mow\" width=350 />
        </div>
        <div class=\"col-md-6\">
            <h2 class=\"titre-perceuse\">Mow</h2>
            <p class=\"presentation-perceuse\">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam facilis explicabo modi, quas nulla natus, quidem qui culpa commodi illo eum atque, sed exercitationem reiciendis perspiciatis voluptatibus temporibus repudiandae enim! Vel doloremque est minima vero ipsa dolorum officia, fuga culpa sapiente temporibus beatae nam vitae et amet cum architecto velit. Nesciunt fuga tempore placeat delectus quos velit expedita exercitationem mollitia accusamus porro. Accusamus voluptatum numquam modi veritatis sit rerum officiis facere iste et quam repellat doloremque tempora nesciunt recusandae obcaecati expedita harum asperiores quis, in iure consequatur pariatur porro sed. Aliquam ipsa praesentium reprehenderit ea odio quia mollitia ipsum. Rem.</p>
        </div>
    </div>

    <div class=\"galerie1\"></div>

    <div class=\"row\">
        <div class=\"col-md-6\">
            <img src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/facebook.png"), "html", null, true);
        echo "\" alt=\"Mow\" width=60 />
        </div>
        <div class=\"col-md-6\">
            <img src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/instagram.png"), "html", null, true);
        echo "\" alt=\"Mow\" width=60 />
        </div>
    </div>
</section>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "qsn/qsn.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 53,  146 => 50,  131 => 38,  122 => 32,  116 => 29,  101 => 17,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello QsnController!{% endblock %}

{% block body %}

<section class=\"ctnr-qsn\">
    
    <div class=\"row qsn-titre\">
        <div class=\"col-12\">
            <h1 class=\"titre-qsn\">Qui sommes nous ?</h1>
        </div>
    </div>
    
    <div class=\"row zone-perceuse\">
        <div class=\"col-md-6\">
            <img src=\"{{ asset('img/mow-tatoueuse.jpeg') }}\" alt=\"Mow\" width=400 />
        </div>
        <div class=\"col-md-6 zone-texte-perceuse\">
            <h2 class=\"titre-perceuse\">Cécyl</h2>
            <p class=\"presentation-perceuse\">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam facilis explicabo modi, quas nulla natus, quidem qui culpa commodi illo eum atque, sed exercitationem reiciendis perspiciatis voluptatibus temporibus repudiandae enim! Vel doloremque est minima vero ipsa dolorum officia, fuga culpa sapiente temporibus beatae nam vitae et amet cum architecto velit. Nesciunt fuga tempore placeat delectus quos velit expedita exercitationem mollitia accusamus porro. Accusamus voluptatum numquam modi veritatis sit rerum officiis facere iste et quam repellat doloremque tempora nesciunt recusandae obcaecati expedita harum asperiores quis, in iure consequatur pariatur porro sed. Aliquam ipsa praesentium reprehenderit ea odio quia mollitia ipsum. Rem.</p>
        </div>
    </div>

    <div class=\"galerie1\"></div>

    <div class=\"row\">
        <div class=\"col-md-6\">
            <img src=\"{{ asset('img/facebook.png') }}\" alt=\"Mow\" width=60 />
        </div>
        <div class=\"col-md-6\">
            <img src=\"{{ asset('img/instagram.png') }}\" alt=\"Mow\" width=60 />
        </div>
    </div>
    
    <div class=\"row\">
        <div class=\"col-md-6\">
            <img src=\"{{ asset('img/mow-tatoueuse.jpeg') }}\" alt=\"Mow\" width=350 />
        </div>
        <div class=\"col-md-6\">
            <h2 class=\"titre-perceuse\">Mow</h2>
            <p class=\"presentation-perceuse\">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam facilis explicabo modi, quas nulla natus, quidem qui culpa commodi illo eum atque, sed exercitationem reiciendis perspiciatis voluptatibus temporibus repudiandae enim! Vel doloremque est minima vero ipsa dolorum officia, fuga culpa sapiente temporibus beatae nam vitae et amet cum architecto velit. Nesciunt fuga tempore placeat delectus quos velit expedita exercitationem mollitia accusamus porro. Accusamus voluptatum numquam modi veritatis sit rerum officiis facere iste et quam repellat doloremque tempora nesciunt recusandae obcaecati expedita harum asperiores quis, in iure consequatur pariatur porro sed. Aliquam ipsa praesentium reprehenderit ea odio quia mollitia ipsum. Rem.</p>
        </div>
    </div>

    <div class=\"galerie1\"></div>

    <div class=\"row\">
        <div class=\"col-md-6\">
            <img src=\"{{ asset('img/facebook.png') }}\" alt=\"Mow\" width=60 />
        </div>
        <div class=\"col-md-6\">
            <img src=\"{{ asset('img/instagram.png') }}\" alt=\"Mow\" width=60 />
        </div>
    </div>
</section>


{% endblock %}
", "qsn/qsn.html.twig", "/home/laurie/Bureau/picsteel/templates/qsn/qsn.html.twig");
    }
}
