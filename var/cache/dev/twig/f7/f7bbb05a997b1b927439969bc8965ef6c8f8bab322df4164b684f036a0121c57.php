<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* faqtattoo/index.html.twig */
class __TwigTemplate_f1a53b3a4625b40fdd8807322bbe9cbfd0297e77ce0fe31f9b6acaad55c0378e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "faqtattoo/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "faqtattoo/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "faqtattoo/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello FaqtattooController!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "div.
<h1>QUESTIONS FREQUENTES TATTOO<h1>

<h4>Je souhaite faire un tatouage avec Mow , quelles démarches effectuer ?</h4>

<p>Tout d'abord, assures toi que ma veine graphique ( création unique Dotwork Blackwork noir et
gris) soit en adéquation avec ton projet.
( je ne fais pas de mini tattoo, ni couleur, ni lettrage, ni date de naissance et autres tattoo lambda
etc).
Tu peux me contacter sur les réseaux sociaux mais privilégies par mail <p mailto:\"mowmccat@hotmail.com\">mowmccat@hotmail.com</p>

<p>* COVID 19 Il est possible de prendre rdv pour discuter projet en physique au shop pour se
rencontrer, merci de me renseigner tes disponibilités , et je te proposerais des créneaux disponible.
Masqué sans accompagnant et avec les informations demandés plus bas.*
Ensuite, pour traiter ta demande tattoo et établir un devis il me faut :
– une description précise et déterminée de ton projet ou le flash qui t'interesse
– la taille approximative du tattoo en centimètres dans l'idéal sachant que je ne fais pas de
petit tattoo
– des photos de références (photos dessins, tattoo existant, peinture, film ect)
– la zone d'emplacement idéal (photo bienvenue quitte à détourer la zone à tatouer)
Toute demande sans ces différents éléments ne pourra malheureusement pas être traitée.
Ensuite si ton projet est en adéquation avec ma veine graphique , est validé et budgetisé il faudra
déposer des ARRHS de 30 % de la somme finale soit via Paypal soit directement à la boutique sur
RDV.</p>

<h4>Est ce que Mow fait du cover ?</h4>
<p>Tout dépend , au mieux il faut prendre rdv pour évaluer les possibilités ou non en fonction du tattoo
existant.</p>

<h4>Est ce que Mow tattoo les mineur(e)s ?</h4>
<p>Non même avec autorisation parentale.</p>

<h4>Est ce que Mow fait du maquillage permanent ?</h4>
<p>Non.</p>

<h4>Est ce que Mow tattoo les mains, les doigts , le visage et toute zone très visible ?</h4>
<p>Tout dépend de mon estimation , si tu es déjà très tatouée, de tes motivations etc... Je me réserve le
droit de refuser ou d'accepter.</p>

<h4>Est ce qu'il est possible de voir le dessin avant de tatouer ?</h4>
<p>Oui nous travaillons ensemble, avant la conception nous échangeons et nous nous mettons d'accord
sur les différents élément avant d'enclencher le projet.
C'est ton corps tu as un droit de regard et de modification ( dans la mesure du raisonnable biensûr)
PARCONTRE Je ne fais pas de dessin gratuitement pour l'exemple, à toi de faire des recherches
suffisamment abouties pour savoir un minimum ce que tu veux.</p>

<h4>A quoi servent les « ARRHS » ?</h4>
<p>Les ARRHS sont en moyenne 30% de la somme finale du tattoo, nécessaire pour bloquer un rdv et
pour me garantir ton engagement , afin de ne pas faire de dessin gratuitement, ne pas perdre de
temps, et surtout éviter les « oublies » et plantage de rdv, car oui si tu ne viens pas sans prévenir tu
voles la place de quelqu'un d'autre. Ils ne sont donc pas remboursables.</p>

<h4>Est ce que le tatouage fait mal ?</h4>
<p>On va pas se mentir : Oui.
Il faut garder à l'esprit que le tatouage est une effraction à la peau , une blessure, une plaie a soigner
et à faire cicatriser. Après tout dépend de ta sensibilité, de tes limites personnelles, ta forme
physique et morale, ton état d'esprit et ta détermination. Manges bien, hydrates toi, dors bien et sois
sur(e) de ton choix et tout se passera bien. Idem pour la cicatrisation.</p>

<h4>Quels sont les contre indications du tatouage ?</h4>
<p>3 semaines sans bain, piscine, mer, sauna, hammam et soleil
3 semaines de soins rigoureux , lavages et crèmages quotidien.
3 semaines avec des phases de croûtes, de démangeaisons et de tiraillement.
Pas de drogues ni d'alcool, avant pendant et après le tattoo.
Attention pas de tatouage sur femme enceinte, sur certaines personnes ayant certaines maladies ,
sous certains traitements médicamenteux et affections de la peau : merci de bien signaler toute
anomalies à Mow, afin d’évaluer si besoin d'un avis médical.</p>

<h4>Combien de temps faut il pour cicatriser ?</h4>
<p>Tout dépend de ton corps et de comment tu t'occupes de ton tattoo.
En moyenne ça prend environ 3 semaines à 1 mois. Mais il faudra continuer de l'hydrater et le
protéger du soleil tout au long de ta vie.</p>

<h4>COVID 19 comment ça se passe ? Est il possible de se faire tatouer ?</h4>
<p>Oui il est possible de se faire tatouer.
> sauf arrêté gouvernemental de fermeture et confinement <
Tout se fait par réseaux sociaux / mail ou uniquement sur rdv au shop,
Peu de choses changent , l'hygiène et les règles sanitaires strictes étant déja la priorité dans le shop/
Seuls le port de masque obligatoire tout le long de la procédure et du gel hydro alcoolique à
disposition pour tous s'ajoutent et pas d'accompagnant.</p>

<p>Si Symptômes ou toutes suspicions avant séance de tattoo merci par respect et bienveillance pour
l'équipe Pic Steel de le signaler, afin de reporter le rdv.</p>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "faqtattoo/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello FaqtattooController!{% endblock %}

{% block body %}
div.
<h1>QUESTIONS FREQUENTES TATTOO<h1>

<h4>Je souhaite faire un tatouage avec Mow , quelles démarches effectuer ?</h4>

<p>Tout d'abord, assures toi que ma veine graphique ( création unique Dotwork Blackwork noir et
gris) soit en adéquation avec ton projet.
( je ne fais pas de mini tattoo, ni couleur, ni lettrage, ni date de naissance et autres tattoo lambda
etc).
Tu peux me contacter sur les réseaux sociaux mais privilégies par mail <p mailto:\"mowmccat@hotmail.com\">mowmccat@hotmail.com</p>

<p>* COVID 19 Il est possible de prendre rdv pour discuter projet en physique au shop pour se
rencontrer, merci de me renseigner tes disponibilités , et je te proposerais des créneaux disponible.
Masqué sans accompagnant et avec les informations demandés plus bas.*
Ensuite, pour traiter ta demande tattoo et établir un devis il me faut :
– une description précise et déterminée de ton projet ou le flash qui t'interesse
– la taille approximative du tattoo en centimètres dans l'idéal sachant que je ne fais pas de
petit tattoo
– des photos de références (photos dessins, tattoo existant, peinture, film ect)
– la zone d'emplacement idéal (photo bienvenue quitte à détourer la zone à tatouer)
Toute demande sans ces différents éléments ne pourra malheureusement pas être traitée.
Ensuite si ton projet est en adéquation avec ma veine graphique , est validé et budgetisé il faudra
déposer des ARRHS de 30 % de la somme finale soit via Paypal soit directement à la boutique sur
RDV.</p>

<h4>Est ce que Mow fait du cover ?</h4>
<p>Tout dépend , au mieux il faut prendre rdv pour évaluer les possibilités ou non en fonction du tattoo
existant.</p>

<h4>Est ce que Mow tattoo les mineur(e)s ?</h4>
<p>Non même avec autorisation parentale.</p>

<h4>Est ce que Mow fait du maquillage permanent ?</h4>
<p>Non.</p>

<h4>Est ce que Mow tattoo les mains, les doigts , le visage et toute zone très visible ?</h4>
<p>Tout dépend de mon estimation , si tu es déjà très tatouée, de tes motivations etc... Je me réserve le
droit de refuser ou d'accepter.</p>

<h4>Est ce qu'il est possible de voir le dessin avant de tatouer ?</h4>
<p>Oui nous travaillons ensemble, avant la conception nous échangeons et nous nous mettons d'accord
sur les différents élément avant d'enclencher le projet.
C'est ton corps tu as un droit de regard et de modification ( dans la mesure du raisonnable biensûr)
PARCONTRE Je ne fais pas de dessin gratuitement pour l'exemple, à toi de faire des recherches
suffisamment abouties pour savoir un minimum ce que tu veux.</p>

<h4>A quoi servent les « ARRHS » ?</h4>
<p>Les ARRHS sont en moyenne 30% de la somme finale du tattoo, nécessaire pour bloquer un rdv et
pour me garantir ton engagement , afin de ne pas faire de dessin gratuitement, ne pas perdre de
temps, et surtout éviter les « oublies » et plantage de rdv, car oui si tu ne viens pas sans prévenir tu
voles la place de quelqu'un d'autre. Ils ne sont donc pas remboursables.</p>

<h4>Est ce que le tatouage fait mal ?</h4>
<p>On va pas se mentir : Oui.
Il faut garder à l'esprit que le tatouage est une effraction à la peau , une blessure, une plaie a soigner
et à faire cicatriser. Après tout dépend de ta sensibilité, de tes limites personnelles, ta forme
physique et morale, ton état d'esprit et ta détermination. Manges bien, hydrates toi, dors bien et sois
sur(e) de ton choix et tout se passera bien. Idem pour la cicatrisation.</p>

<h4>Quels sont les contre indications du tatouage ?</h4>
<p>3 semaines sans bain, piscine, mer, sauna, hammam et soleil
3 semaines de soins rigoureux , lavages et crèmages quotidien.
3 semaines avec des phases de croûtes, de démangeaisons et de tiraillement.
Pas de drogues ni d'alcool, avant pendant et après le tattoo.
Attention pas de tatouage sur femme enceinte, sur certaines personnes ayant certaines maladies ,
sous certains traitements médicamenteux et affections de la peau : merci de bien signaler toute
anomalies à Mow, afin d’évaluer si besoin d'un avis médical.</p>

<h4>Combien de temps faut il pour cicatriser ?</h4>
<p>Tout dépend de ton corps et de comment tu t'occupes de ton tattoo.
En moyenne ça prend environ 3 semaines à 1 mois. Mais il faudra continuer de l'hydrater et le
protéger du soleil tout au long de ta vie.</p>

<h4>COVID 19 comment ça se passe ? Est il possible de se faire tatouer ?</h4>
<p>Oui il est possible de se faire tatouer.
> sauf arrêté gouvernemental de fermeture et confinement <
Tout se fait par réseaux sociaux / mail ou uniquement sur rdv au shop,
Peu de choses changent , l'hygiène et les règles sanitaires strictes étant déja la priorité dans le shop/
Seuls le port de masque obligatoire tout le long de la procédure et du gel hydro alcoolique à
disposition pour tous s'ajoutent et pas d'accompagnant.</p>

<p>Si Symptômes ou toutes suspicions avant séance de tattoo merci par respect et bienveillance pour
l'équipe Pic Steel de le signaler, afin de reporter le rdv.</p>
{% endblock %}
", "faqtattoo/index.html.twig", "/home/laurie/Bureau/picsteel/templates/faqtattoo/index.html.twig");
    }
}
