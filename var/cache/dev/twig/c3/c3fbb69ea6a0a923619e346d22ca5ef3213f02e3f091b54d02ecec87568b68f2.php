<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* footheader/header.html.twig */
class __TwigTemplate_06b6fb79c7266f96a0089152f37ba73b6ce942d6d2db793cd0bece3c8de4250b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "footheader/header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "footheader/header.html.twig"));

        // line 1
        echo "

<div class=\"img\">
<div id=\"menu\">
<nav    class=\"navbar navbar-expand-lg navbar-light bg-light\">
  
  
  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
    <span class=\"navbar-toggler-icon\"></span>
    </button>
      <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
    <ul class=\"navbar-nav mr-auto\">
      <li class=\"nav-item \">
        <a class=\"nav-link\" href=\"#\"> <span class=\"loos\">Piercings</span> <span class=\"sr-only\">(current)</span></a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\"><span class=\"loos\">Tatouages</span></a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\"><span class=\"loos\">Qui sommes-nous</span></a>
      </li>
    <div class=\"nav-item\">
        <a  id=\"nopadd\" class=\"nav-link\" href=\"\"><img  class=\"imgheader\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/logoheader.png"), "html", null, true);
        echo "\"></a>
      </div>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\"><span class=\"loos\">Soins</span></a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\"><span class=\"loos\">FAQ</span></a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\"><span class=\"loos\">Contact<span></a>
      </li>
    </ul>

  </div>
</nav>
</div>

</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "footheader/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 23,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("

<div class=\"img\">
<div id=\"menu\">
<nav    class=\"navbar navbar-expand-lg navbar-light bg-light\">
  
  
  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
    <span class=\"navbar-toggler-icon\"></span>
    </button>
      <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
    <ul class=\"navbar-nav mr-auto\">
      <li class=\"nav-item \">
        <a class=\"nav-link\" href=\"#\"> <span class=\"loos\">Piercings</span> <span class=\"sr-only\">(current)</span></a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\"><span class=\"loos\">Tatouages</span></a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\"><span class=\"loos\">Qui sommes-nous</span></a>
      </li>
    <div class=\"nav-item\">
        <a  id=\"nopadd\" class=\"nav-link\" href=\"\"><img  class=\"imgheader\" src=\"{{asset('/img/logoheader.png')}}\"></a>
      </div>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\"><span class=\"loos\">Soins</span></a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\"><span class=\"loos\">FAQ</span></a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\"><span class=\"loos\">Contact<span></a>
      </li>
    </ul>

  </div>
</nav>
</div>

</div>", "footheader/header.html.twig", "/home/laurie/Bureau/picsteel/templates/footheader/header.html.twig");
    }
}
