<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* tatouages/index.html.twig */
class __TwigTemplate_087d60aa47fca00cc764c0ca21560ea72b5af7f70e8a580d03c1fbad3455aa98 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "tatouages/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "tatouages/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "tatouages/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Tatouages
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "
<!-- tabs sous menu !-->
<ul class=\"nav nav-tabs\">
    <li class=\"active\">
        <a class=\"men\" data-toggle=\"tab\" href=\"#tatouages\">Tatouages</a>
    </li>
    <li>
        <a class=\"men\" data-toggle=\"tab\" href=\"#prints\">Prints</a>
    </li>
    <li>
        <a class=\"men\" data-toggle=\"tab\" href=\"#flash\">Flashs</a>
    </li>
    <li>
        <a class=\"men\" data-toggle=\"tab\" href=\"#devis\">Devis</a>
    </li>
    <li>
        <a class=\"men\" data-toggle=\"tab\" href=\"#soin\">Soin</a>
    </li>
    <li>
        <a class=\"men\" data-toggle=\"tab\" href=\"#faq\">FAQ</a>
    </li>
</ul>

<!-- end tabs !-->

<!-- all my tab content start here !-->
<div class=\"tab-content\">
    <div id=\"tatouages\" class=\"tab-pane fade-active\">
        <div class=\"col-md-12 debtat\">
            <div class=\"Pagetatouages\">
                <div id=\"\" class=\"col-md-4 offset-md-2\">
                    <img class=\"imgtatouages\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/piercing1.jpg"), "html", null, true);
        echo "\"></div>
                <div id=\"tatouagescol\" class=\"col-md-4\">
                    <p class=\"descript\">
                        <a>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem
                            ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim .\"</a>
                    </p>
                </div>
            </div>
        </div>
        <div class=\"col-md-12 galerie\"></div>
        <div class=\"row align-self-center\">
            <div class=\"col-md-12\" id=\"projects\">
                <h1 class=\"projets\">
                    Projets disponibles
                </h1>
                <button class=\"btnproj\" type=\"button\">Prints</button>
                <button class=\"btnproj\" type=\"button\">Flashs</button>
                <button class=\"btnproj\" type=\"button\">Devis</button>
            </div>
        </div>
        <div class=\"Pagetatouages2\">
            <div id=\"\" class=\"col-md-4 offset-md-1\">
                <img class=\"imgtatouages2\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/piercing1.jpg"), "html", null, true);
        echo "\"></div>
            <div id=\"tatouagescol2\" class=\"col-md-4\">
                <h2 class=\"mow\">Mow</h2>
                <p class=\"descript\">
                    <a>\"Lorem ipsum dolor sit amet,Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                        dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                        qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                        minim .\"</a>
                </p>
            </div>
        </div>
        <div class=\"col-md-12 offset-md-5 \">
            <button class=\"btnrdv\" type=\"button\">Qui sommes-nous?</button>
        </div>
    </div>

   <!-- all the prints content !-->
    <div id=\"prints\" class=\"tab-pane fade\">
        <div class=\"container\">
            <h2>Tous les prints ci-dessous sont disponibles. </h2>
            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print1.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print2.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print3.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print4.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print5.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print6.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print7.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print8.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print10.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print11.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print12.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print13.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print14.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 227
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print15.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

<!-- prints finished !-->
<!-- flashs start content !-->
<div id=\"flash\" class=\"tab-pane fade\">
<div class=\"container\">
            <h2>Tous les flashs ci-dessous sont disponibles. </h2>
            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 248
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 257
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print1.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 266
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print2.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 278
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print3.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 287
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print4.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 296
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print5.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 309
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print6.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 318
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print7.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 327
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print8.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 339
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print10.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 348
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print11.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 357
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print12.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 368
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print13.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 377
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print14.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"";
        // line 386
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/print15.jpeg"), "html", null, true);
        echo "\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>


            </div>
        </div>

</div>
<!-- flashs end !-->
<!-- devis start !-->
            <div id=\"devis\" class=\"tab-pane fade\">
            </div>
<!-- devis end !-->
<!-- soin start !-->
            <div id=\"soin\" class=\"tab-pane fade\">
            </div>
<!-- soin end !-->
<!-- faq start !-->
            <div id=\"faq\" class=\"tab-pane fade\">
           <div class=\"container\">
  <div id=\"accordion\" class=\"panel-group\">
    <div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBodyOne\" class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\">Je souhaite faire un tatouage avec Mow, quelles démarches effectuer ?</a>
        </h4>
      </div>
      <div id=\"panelBodyOne\" class=\"panel-collapse collapse in\">
      <div class=\"panel-body\">
          <p>Tout d'abord, assures toi que ma veine graphique ( création unique Dotwork Blackwork noir et gris) soit en adéquation avec ton projet. ( je ne fais pas de mini tattoo, ni couleur, ni lettrage, ni date de naissance et autres tattoo lambda etc). 
Tu peux me contacter sur les réseaux sociaux mais privilégies par mail <strong>mowmccat@hotmail.com</strong> 
* COVID 19 Il est possible de prendre rdv pour discuter projet en physique au shop pour se rencontrer, merci de me renseigner tes disponibilités , et je te proposerais des créneaux disponible. Masqué sans accompagnant et avec les informations demandés plus bas.* 
Ensuite, pour traiter ta demande tattoo et établir un devis il me faut : 
– une description précise et déterminée de ton projet ou le flash qui t'interesse – la taille approximative du tattoo en centimètres dans l'idéal sachant que je ne fais pas de 
petit tattoo – des photos de références (photos dessins, tattoo existant, peinture, film ect) – la zone d'emplacement idéal (photo bienvenue quitte à détourer la zone à tatouer) 
Toute demande sans ces différents éléments ne pourra malheureusement pas être traitée. 
Ensuite si ton projet est en adéquation avec ma veine graphique , est validé et budgetisé il faudra déposer des ARRHS de 30 % de la somme finale soit via Paypal soit directement à la boutique sur RDV. 
</p>
        </div>
      </div>
    </div>
    <div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBodyTwo\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce que Mow fait du cover ?</a>
        </h4>
      </div>
      <div id=\"panelBodyTwo\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Tout dépend , au mieux il faut prendre rdv pour évaluer les possibilités ou non en fonction du tattoo existant.</p>
        </div>
      </div>
    </div>
    <div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBodyThree\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce que Mow tattoo les mineur(e)s ?</a>
        </h4>
      </div>
      <div id=\"panelBodyThree\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Non même avec autorisation parentale.</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBodyFour\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce que Mow fait du maquillage permanent ?</a>
        </h4>
      </div>
      <div id=\"panelBodyFour\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Non.</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody5\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce que Mow tattoo les mains, les doigts , le visage et toute zone très visible ?</a>
        </h4>
      </div>
      <div id=\"panelBody5\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Tout dépend de mon estimation , si tu es déjà très tatouée, de tes motivations etc... Je me réserve le droit de refuser ou d'accepter.</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody6\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce qu'il est possible de voir le dessin avant de tatouer ?</a>
        </h4>
      </div>
      <div id=\"panelBody6\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Oui nous travaillons ensemble, avant la conception nous échangeons et nous nous mettons d'accord sur les différents élément avant d'enclencher le projet. C'est ton corps tu as un droit de regard et de modification ( dans la mesure du raisonnable biensur) PARCONTRE Je ne fais pas de dessin gratuitement pour l'exemple, à toi de faire des recherches suffisamment abouties pour savoir un minimum ce que tu veux.
</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody7\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">A quoi servent les « ARRHS » ?</a>
        </h4>
      </div>
      <div id=\"panelBody7\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Les ARRHS sont en moyenne 30% de la somme finale du tattoo, nécessaire pour bloquer un rdv et pour me garantir ton engagement , afin de ne pas faire de dessin gratuitement, ne pas perdre de temps, et surtout éviter les « oublies » et plantage de rdv, car oui si tu ne viens pas sans prévenir tu voles la place de quelqu'un d'autre. Ils ne sont donc pas remboursables. </p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody8\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce que le tatouage fait mal ?</a>
        </h4>
      </div>
      <div id=\"panelBody8\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>On va pas se mentir : Oui. Il faut garder à l'esprit que le tatouage est une effraction à la peau , une blessure, une plaie a soigner et à faire cicatriser. Après tout dépend de ta sensibilité, de tes limites personnelles, ta forme physique et morale, ton état d'esprit et ta détermination. Manges bien, hydrates toi, dors bien et sois sur(e) de ton choix et tout se passera bien. Idem pour la cicatrisation. </p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody9\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Quels sont les contre indications du tatouage ?</a>
        </h4>
      </div>
      <div id=\"panelBody9\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>3 semaines sans bain, piscine, mer, sauna, hammam et soleil 3 semaines de soins rigoureux , lavages et crèmages quotidien. 3 semaines avec des phases de croûtes, de démangeaisons et de tiraillement. Pas de drogues ni d'alcool, avant pendant et après le tattoo. Attention pas de tatouage sur femme enceinte, sur certaines personnes ayant certaines maladies , sous certains traitements médicamenteux et affections de la peau : merci de bien signaler toute anomalies à Mow, afin d’évaluer si besoin d'un avis médical. 
</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody10\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Combien de temps faut il pour cicatriser ?</a>
        </h4>
      </div>
      <div id=\"panelBody10\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Tout dépend de ton corps et de comment tu t'occupes de ton tattoo. En moyenne ça prend environ 3 semaines à 1 mois. Mais il faudra continuer de l'hydrater et le protéger du soleil tout au long de ta vie. 
</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody11\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">COVID 19 comment ça se passe ? Est il possible de se faire tatouer ?</a>
        </h4>
      </div>
      <div id=\"panelBody11\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Oui il est possible de se faire tatouer.
> sauf arrêté gouvernemental de fermeture et confinement < Tout se fait par réseaux sociaux / mail ou uniquement sur rdv au shop, Peu de choses changent , l'hygiène et les règles sanitaires strictes étant déja la priorité dans le shop/ Seuls le port de masque obligatoire tout le long de la procédure et du gel hydro alcoolique à disposition pour tous s'ajoutent et pas d'accompagnant. 
Si Symptômes ou toutes suspicions avant séance de tattoo merci par respect et bienveillance pour l'équipe Pic Steel de le signaler, afin de reporter le rdv. 
</p>
        </div>
      </div>
    </div>
  </div>
</div>
            </div>
<!-- faq end !-->
        </div>
    </div>
\t</div>

\t<!-- all my tab content finish here !-->


    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "tatouages/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  563 => 386,  551 => 377,  539 => 368,  525 => 357,  513 => 348,  501 => 339,  486 => 327,  474 => 318,  462 => 309,  446 => 296,  434 => 287,  422 => 278,  407 => 266,  395 => 257,  383 => 248,  359 => 227,  347 => 218,  335 => 209,  321 => 198,  309 => 189,  297 => 180,  282 => 168,  270 => 159,  258 => 150,  242 => 137,  230 => 128,  218 => 119,  203 => 107,  191 => 98,  179 => 89,  152 => 65,  122 => 38,  89 => 7,  79 => 6,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Tatouages
{% endblock %}

{% block body %}

<!-- tabs sous menu !-->
<ul class=\"nav nav-tabs\">
    <li class=\"active\">
        <a class=\"men\" data-toggle=\"tab\" href=\"#tatouages\">Tatouages</a>
    </li>
    <li>
        <a class=\"men\" data-toggle=\"tab\" href=\"#prints\">Prints</a>
    </li>
    <li>
        <a class=\"men\" data-toggle=\"tab\" href=\"#flash\">Flashs</a>
    </li>
    <li>
        <a class=\"men\" data-toggle=\"tab\" href=\"#devis\">Devis</a>
    </li>
    <li>
        <a class=\"men\" data-toggle=\"tab\" href=\"#soin\">Soin</a>
    </li>
    <li>
        <a class=\"men\" data-toggle=\"tab\" href=\"#faq\">FAQ</a>
    </li>
</ul>

<!-- end tabs !-->

<!-- all my tab content start here !-->
<div class=\"tab-content\">
    <div id=\"tatouages\" class=\"tab-pane fade-active\">
        <div class=\"col-md-12 debtat\">
            <div class=\"Pagetatouages\">
                <div id=\"\" class=\"col-md-4 offset-md-2\">
                    <img class=\"imgtatouages\" src=\"{{ asset('/img/piercing1.jpg')}}\"></div>
                <div id=\"tatouagescol\" class=\"col-md-4\">
                    <p class=\"descript\">
                        <a>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem
                            ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim .\"</a>
                    </p>
                </div>
            </div>
        </div>
        <div class=\"col-md-12 galerie\"></div>
        <div class=\"row align-self-center\">
            <div class=\"col-md-12\" id=\"projects\">
                <h1 class=\"projets\">
                    Projets disponibles
                </h1>
                <button class=\"btnproj\" type=\"button\">Prints</button>
                <button class=\"btnproj\" type=\"button\">Flashs</button>
                <button class=\"btnproj\" type=\"button\">Devis</button>
            </div>
        </div>
        <div class=\"Pagetatouages2\">
            <div id=\"\" class=\"col-md-4 offset-md-1\">
                <img class=\"imgtatouages2\" src=\"{{ asset('/img/piercing1.jpg')}}\"></div>
            <div id=\"tatouagescol2\" class=\"col-md-4\">
                <h2 class=\"mow\">Mow</h2>
                <p class=\"descript\">
                    <a>\"Lorem ipsum dolor sit amet,Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                        dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                        qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                        minim .\"</a>
                </p>
            </div>
        </div>
        <div class=\"col-md-12 offset-md-5 \">
            <button class=\"btnrdv\" type=\"button\">Qui sommes-nous?</button>
        </div>
    </div>

   <!-- all the prints content !-->
    <div id=\"prints\" class=\"tab-pane fade\">
        <div class=\"container\">
            <h2>Tous les prints ci-dessous sont disponibles. </h2>
            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print1.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print2.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print3.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print4.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print5.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print6.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print7.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print8.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print10.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print11.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print12.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print13.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print14.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print15.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

<!-- prints finished !-->
<!-- flashs start content !-->
<div id=\"flash\" class=\"tab-pane fade\">
<div class=\"container\">
            <h2>Tous les flashs ci-dessous sont disponibles. </h2>
            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print1.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print2.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print3.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print4.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print5.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print6.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print7.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print8.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print10.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print11.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print12.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"row mb-5\">
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print13.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print14.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"card\">
                        <img src=\"{{ asset('/img/print15.jpeg')}}\" class=\"card-img-top\" alt=\"...\">
                        <div class=\"card-body\">
                            <p class=\"card-text\">A5 10 euros / A4 15 euros / A3 20 euros</p>
                            <a href=\"#\" class=\"btn btn-sm btn-primary\" id=\"btnprints\">Je veux</a>
                        </div>
                    </div>
                </div>


            </div>
        </div>

</div>
<!-- flashs end !-->
<!-- devis start !-->
            <div id=\"devis\" class=\"tab-pane fade\">
            </div>
<!-- devis end !-->
<!-- soin start !-->
            <div id=\"soin\" class=\"tab-pane fade\">
            </div>
<!-- soin end !-->
<!-- faq start !-->
            <div id=\"faq\" class=\"tab-pane fade\">
           <div class=\"container\">
  <div id=\"accordion\" class=\"panel-group\">
    <div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBodyOne\" class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\">Je souhaite faire un tatouage avec Mow, quelles démarches effectuer ?</a>
        </h4>
      </div>
      <div id=\"panelBodyOne\" class=\"panel-collapse collapse in\">
      <div class=\"panel-body\">
          <p>Tout d'abord, assures toi que ma veine graphique ( création unique Dotwork Blackwork noir et gris) soit en adéquation avec ton projet. ( je ne fais pas de mini tattoo, ni couleur, ni lettrage, ni date de naissance et autres tattoo lambda etc). 
Tu peux me contacter sur les réseaux sociaux mais privilégies par mail <strong>mowmccat@hotmail.com</strong> 
* COVID 19 Il est possible de prendre rdv pour discuter projet en physique au shop pour se rencontrer, merci de me renseigner tes disponibilités , et je te proposerais des créneaux disponible. Masqué sans accompagnant et avec les informations demandés plus bas.* 
Ensuite, pour traiter ta demande tattoo et établir un devis il me faut : 
– une description précise et déterminée de ton projet ou le flash qui t'interesse – la taille approximative du tattoo en centimètres dans l'idéal sachant que je ne fais pas de 
petit tattoo – des photos de références (photos dessins, tattoo existant, peinture, film ect) – la zone d'emplacement idéal (photo bienvenue quitte à détourer la zone à tatouer) 
Toute demande sans ces différents éléments ne pourra malheureusement pas être traitée. 
Ensuite si ton projet est en adéquation avec ma veine graphique , est validé et budgetisé il faudra déposer des ARRHS de 30 % de la somme finale soit via Paypal soit directement à la boutique sur RDV. 
</p>
        </div>
      </div>
    </div>
    <div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBodyTwo\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce que Mow fait du cover ?</a>
        </h4>
      </div>
      <div id=\"panelBodyTwo\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Tout dépend , au mieux il faut prendre rdv pour évaluer les possibilités ou non en fonction du tattoo existant.</p>
        </div>
      </div>
    </div>
    <div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBodyThree\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce que Mow tattoo les mineur(e)s ?</a>
        </h4>
      </div>
      <div id=\"panelBodyThree\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Non même avec autorisation parentale.</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBodyFour\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce que Mow fait du maquillage permanent ?</a>
        </h4>
      </div>
      <div id=\"panelBodyFour\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Non.</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody5\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce que Mow tattoo les mains, les doigts , le visage et toute zone très visible ?</a>
        </h4>
      </div>
      <div id=\"panelBody5\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Tout dépend de mon estimation , si tu es déjà très tatouée, de tes motivations etc... Je me réserve le droit de refuser ou d'accepter.</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody6\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce qu'il est possible de voir le dessin avant de tatouer ?</a>
        </h4>
      </div>
      <div id=\"panelBody6\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Oui nous travaillons ensemble, avant la conception nous échangeons et nous nous mettons d'accord sur les différents élément avant d'enclencher le projet. C'est ton corps tu as un droit de regard et de modification ( dans la mesure du raisonnable biensur) PARCONTRE Je ne fais pas de dessin gratuitement pour l'exemple, à toi de faire des recherches suffisamment abouties pour savoir un minimum ce que tu veux.
</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody7\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">A quoi servent les « ARRHS » ?</a>
        </h4>
      </div>
      <div id=\"panelBody7\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Les ARRHS sont en moyenne 30% de la somme finale du tattoo, nécessaire pour bloquer un rdv et pour me garantir ton engagement , afin de ne pas faire de dessin gratuitement, ne pas perdre de temps, et surtout éviter les « oublies » et plantage de rdv, car oui si tu ne viens pas sans prévenir tu voles la place de quelqu'un d'autre. Ils ne sont donc pas remboursables. </p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody8\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Est ce que le tatouage fait mal ?</a>
        </h4>
      </div>
      <div id=\"panelBody8\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>On va pas se mentir : Oui. Il faut garder à l'esprit que le tatouage est une effraction à la peau , une blessure, une plaie a soigner et à faire cicatriser. Après tout dépend de ta sensibilité, de tes limites personnelles, ta forme physique et morale, ton état d'esprit et ta détermination. Manges bien, hydrates toi, dors bien et sois sur(e) de ton choix et tout se passera bien. Idem pour la cicatrisation. </p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody9\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Quels sont les contre indications du tatouage ?</a>
        </h4>
      </div>
      <div id=\"panelBody9\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>3 semaines sans bain, piscine, mer, sauna, hammam et soleil 3 semaines de soins rigoureux , lavages et crèmages quotidien. 3 semaines avec des phases de croûtes, de démangeaisons et de tiraillement. Pas de drogues ni d'alcool, avant pendant et après le tattoo. Attention pas de tatouage sur femme enceinte, sur certaines personnes ayant certaines maladies , sous certains traitements médicamenteux et affections de la peau : merci de bien signaler toute anomalies à Mow, afin d’évaluer si besoin d'un avis médical. 
</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody10\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">Combien de temps faut il pour cicatriser ?</a>
        </h4>
      </div>
      <div id=\"panelBody10\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Tout dépend de ton corps et de comment tu t'occupes de ton tattoo. En moyenne ça prend environ 3 semaines à 1 mois. Mais il faudra continuer de l'hydrater et le protéger du soleil tout au long de ta vie. 
</p>
        </div>
      </div>
    </div>

\t<div class=\"panel\">
      <div class=\"panel-heading\">
      <h4 class=\"panel-title\">
        <a href=\"#panelBody11\" class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\">COVID 19 comment ça se passe ? Est il possible de se faire tatouer ?</a>
        </h4>
      </div>
      <div id=\"panelBody11\" class=\"panel-collapse collapse\">
      <div class=\"panel-body\">
          <p>Oui il est possible de se faire tatouer.
> sauf arrêté gouvernemental de fermeture et confinement < Tout se fait par réseaux sociaux / mail ou uniquement sur rdv au shop, Peu de choses changent , l'hygiène et les règles sanitaires strictes étant déja la priorité dans le shop/ Seuls le port de masque obligatoire tout le long de la procédure et du gel hydro alcoolique à disposition pour tous s'ajoutent et pas d'accompagnant. 
Si Symptômes ou toutes suspicions avant séance de tattoo merci par respect et bienveillance pour l'équipe Pic Steel de le signaler, afin de reporter le rdv. 
</p>
        </div>
      </div>
    </div>
  </div>
</div>
            </div>
<!-- faq end !-->
        </div>
    </div>
\t</div>

\t<!-- all my tab content finish here !-->


    {% endblock %}

", "tatouages/index.html.twig", "/home/laurie/Bureau/picsteel/templates/tatouages/index.html.twig");
    }
}
