<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* footheader/footer.html.twig */
class __TwigTemplate_eab11c218176fe4ed2097f24dce30a9d9959ac89e71499dd6ef6801f375d834b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "footheader/footer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "footheader/footer.html.twig"));

        // line 1
        echo "<div id=\"width\" class=\"row\">

    <div id=\"width2\" class=\"col-md-12\">

        <div class=\"foot\">
            <div id=\"padd\" class=\"col-md-1\"> <img  class=\"imgheader\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/logoheader.png"), "html", null, true);
        echo "\"></div>
            <div id=\"footcol\" class=\"col-md-3 offset-md-2\">
                <p class=\"color\"><a>Notre Salon</a></p>
                <p class=\"color\"><a>Texte à Add </a></p>
                <br>
                <p class=\"color\"><a>Suivre la communauté PicSteel</a> </p>
                <div><img class=\"imgrs\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/facebook.png"), "html", null, true);
        echo "\">   <img class=\"imgrs\" src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/instagram.png"), "html", null, true);
        echo "\"> </div>

            </div>
            <div id=\"footcol2\" class=\"col-md-3 offset-md-2\">
                <p class=\"color\"><a>Qui Sommes-nous</a></p>
                <p class=\"color\"><a>Texte à Add </a></p>
                
               

            </div>
        </div>

    </div>

</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "footheader/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 12,  50 => 6,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div id=\"width\" class=\"row\">

    <div id=\"width2\" class=\"col-md-12\">

        <div class=\"foot\">
            <div id=\"padd\" class=\"col-md-1\"> <img  class=\"imgheader\" src=\"{{ asset('/img/logoheader.png')}}\"></div>
            <div id=\"footcol\" class=\"col-md-3 offset-md-2\">
                <p class=\"color\"><a>Notre Salon</a></p>
                <p class=\"color\"><a>Texte à Add </a></p>
                <br>
                <p class=\"color\"><a>Suivre la communauté PicSteel</a> </p>
                <div><img class=\"imgrs\" src=\"{{asset('img/facebook.png')}}\">   <img class=\"imgrs\" src=\"{{asset('img/instagram.png')}}\"> </div>

            </div>
            <div id=\"footcol2\" class=\"col-md-3 offset-md-2\">
                <p class=\"color\"><a>Qui Sommes-nous</a></p>
                <p class=\"color\"><a>Texte à Add </a></p>
                
               

            </div>
        </div>

    </div>

</div>", "footheader/footer.html.twig", "/home/laurie/Bureau/picsteel/templates/footheader/footer.html.twig");
    }
}
